﻿using UnityEngine;
using System.Collections;

public class ShowReq : MonoBehaviour {

	[SerializeField] private GameObject bubble;
	[SerializeField] private GameObject feather;
	[SerializeField] private GameObject amount;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			//Display requirements
			bubble.renderer.enabled = true;
			feather.renderer.enabled = true;
			amount.renderer.enabled = true;
		}
	}


	void OnTriggerExit2D (Collider2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			//Display requirements
			bubble.renderer.enabled = false;
			feather.renderer.enabled = false;
			amount.renderer.enabled = false;
		}
	}
}
