﻿using UnityEngine;
using System.Collections;

public class Light_platform : MonoBehaviour {

	private GameObject player; //Use the Satan gameobject



	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player");
	}

	bool timer = true;	//Determines whether fixed update can be called (prior to this there were errors with WaitForSeconds
	
	void FixedUpdate () {

		if (timer == true) 
		{
				// when the player jumps
				if (player.rigidbody2D.velocity.y > 0) 
				{
						//ignore platforms
						collider2D.enabled = false;
				} 
				else 
				{	
						collider2D.enabled = true;
				}

				//when the player presses down key
				if (Input.GetKeyDown (KeyCode.DownArrow)) 
				{
						timer = false;
						StartCoroutine (DropDown (0.2f));
				}
		}


	}

	IEnumerator DropDown(float waitTime) 
	{
			collider2D.enabled = false;
			yield return new WaitForSeconds(waitTime);
			collider2D.enabled = true;
			timer = true;
	}

	// REFERENCE
	// The Coroutine and WaitForSeconds functions were created using the API provided by unity.
	// Date accessed: Monday 21th April
	// URL (WaitForSeconds): http://docs.unity3d.com/Documentation/ScriptReference/WaitForSeconds.html
	// URL (Coroutine): http://docs.unity3d.com/Documentation/ScriptReference/MonoBehaviour.StartCoroutine.html



	
}
