﻿using UnityEngine;
using System.Collections;

public class StackEms5 : MonoBehaviour {
	
	//Bubble
	[SerializeField] private GameObject bubble;
	
	//Text Lines
	[SerializeField] private GameObject text1;
	[SerializeField] private GameObject text2;
	[SerializeField] private GameObject text21;
	[SerializeField] private GameObject text3;
	[SerializeField] private GameObject text31;
	[SerializeField] private GameObject text4;
	[SerializeField] private GameObject text41;

	//Scene elements
	[SerializeField] private GameObject player;
	[SerializeField] private GameObject stackemstalk;
	[SerializeField] private GameObject stackemsblack;
	[SerializeField] private GameObject satancry;
	[SerializeField] private GameObject satannormal;
	[SerializeField] private GameObject satansmile;
	[SerializeField] private GameObject Dim;
	
	//Text Orders
	bool isEnabled;
	bool frame2;
	bool frame3;
	bool frame4;
	bool exit;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
		if (isEnabled) 
		{
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				if (frame2)
				{
					//deactivate frame 1
					text1.gameObject.renderer.enabled = false;

					satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-1);
					
					stackemstalk.renderer.enabled = true;
					stackemsblack.renderer.enabled = false;
					
					//activate frame 2
					text2.gameObject.renderer.enabled = true;
					text21.gameObject.renderer.enabled = true;

					frame2 = false;
					frame3 = true;
				}
				else if (frame3)
				{
					//deactivate frame 1
					text2.gameObject.renderer.enabled = false;
					text21.gameObject.renderer.enabled = false;

					//activate frame 2
					text3.gameObject.renderer.enabled = true;
					text31.gameObject.renderer.enabled = true;

					frame3 = false;
					frame4 = true;
				}
				else if (frame4)
				{
					//deactivate frame 1
					text3.gameObject.renderer.enabled = false;
					text31.gameObject.renderer.enabled = false;

					satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-5);
					
					stackemstalk.renderer.enabled = false;
					stackemsblack.renderer.enabled = true;
					
					//activate frame 2
					text4.gameObject.renderer.enabled = true;
					text41.gameObject.renderer.enabled = true;
					
					
					frame4 = false;
					exit = true;
				}

				else if (exit)
				{
					//deactivate frame 2
					text4.gameObject.renderer.enabled = false;
					text41.gameObject.renderer.enabled = false;
					
					//close bubble
					bubble.gameObject.renderer.enabled = false;
					
					//disable script
					isEnabled = false;
					
					//close minister
					satannormal.renderer.enabled = false;
					stackemstalk.renderer.enabled = false;
					stackemsblack.renderer.enabled = false;
					
					
					Dim.gameObject.renderer.enabled = false;
					
					//resume time and prevent superjumping
					Time.timeScale = 1;
					player.rigidbody2D.isKinematic = false;
					
				}
			}
		}
		
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			
			//pause time and prevent superjumping
			Time.timeScale = 0;
			other.rigidbody2D.isKinematic = true;
			
			Dim.gameObject.renderer.enabled = true;
			satannormal.renderer.enabled = true;
			stackemsblack.renderer.enabled = true;
			stackemstalk.transform.position = new Vector3 (stackemstalk.transform.position.x, stackemstalk.transform.position.y,-5);
			stackemsblack.transform.position = new Vector3 (stackemsblack.transform.position.x, stackemsblack.transform.position.y,-5);
			
			satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-5);
			
			//frame 1
			bubble.gameObject.renderer.enabled = true;
			text1.gameObject.renderer.enabled = true;
			
			
			Destroy(gameObject.collider2D);
			isEnabled = true;
			
			frame2 = true; //play second frame
		}
	}
	
}



