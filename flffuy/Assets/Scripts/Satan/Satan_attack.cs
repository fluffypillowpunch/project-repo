﻿using UnityEngine;
using System.Collections;

public class Satan_attack : MonoBehaviour {
	
	bool attackCD = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		bool attack = Input.GetKeyDown (KeyCode.Z);

			if (attack && !attackCD) 
			{
			StartCoroutine(attackPause(0.2f));
			} 
			else
				collider2D.enabled = false; 


	}

	IEnumerator attackPause(float waitTime) 
	{
		collider2D.enabled = true; 
		attackCD = true;
		yield return new WaitForSeconds(waitTime);
		attackCD = false;
	}
}
