﻿using UnityEngine;
using System.Collections;

public class GoToEnding : MonoBehaviour {

	[SerializeField] private GameObject MainLevels;

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			Destroy (MainLevels);
			GameObject Ending = Instantiate(Resources.Load("Ending")) as GameObject;
		}

	}
}
