﻿using UnityEngine;
using System.Collections;

public class Monster2_animate : MonoBehaviour {

	Animator anim;
	bool canAttack = true;

	//Combat
	float monster2HP = 7;
	bool canBeAttacked = true;
	public GameObject bluePow;

	[SerializeField] private GameObject smoke1;
	[SerializeField] private GameObject smoke2;
	[SerializeField] private GameObject bouncer;

	AudioSource pillowHit;
	AudioSource pillowthrow;


	//movement
	bool walk = true;
	bool walkRight = false;
	bool facingRight = false;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		pillowHit = GetComponent<AudioSource> ();
		pillowthrow = bouncer.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {

			if (walk) 
			{
				WalkCycle ();
				anim.SetBool ("Walk", true);
			} 
			else 
			{	
				anim.SetBool ("Walk", false);
				rigidbody2D.velocity = new Vector2 (0, 0);
			}	

		if (monster2HP <= 0) {
			StartCoroutine (spriteDie (0.5f));
		}

	}

	void WalkCycle()
	{
		if (walkRight) 
		{
			//walk right till barrier hit
			rigidbody2D.velocity = new Vector2 (1, rigidbody2D.velocity.y);

		}
		if (!walkRight)
		{
			rigidbody2D.velocity = new Vector2 (-1, rigidbody2D.velocity.y);
		}
	}

	
	void OnTriggerEnter2D(Collider2D other)
	{

		if (other.gameObject.tag == "Player" && canAttack)
		{
			walk = false;
			rigidbody2D.velocity = new Vector2 (0, rigidbody2D.velocity.y);
			canAttack = false;
			StartCoroutine(Attacking ());
		}

		if (other.gameObject.tag == "RightM2Collider") 
		{
			walkRight = false;
			Flip ();
		}

		if (other.gameObject.tag == "LeftM2Collider") 
		{
			walkRight = true;
			Flip ();
		}

		//Trigger being hit
		if (other.gameObject.tag == "PillowAttackingCollider")
		{
			//Trigger being hit
			if (monster2HP > 0 && canBeAttacked) 
			{
				monster2HP-=3;
				StartCoroutine (spriteSwitch (0.3f));
				StartCoroutine (attackedAnim (0.3f));
				StartCoroutine (canBeAttackedTimer(0.5f));
				pillowHit.Play ();
			}

		} 

		//Trigger being hit
		if (other.gameObject.tag == "Pillow")
		{
			//Trigger being hit
			if (monster2HP > 0 && canBeAttacked) 
			{
				pillowthrow.Play();
				monster2HP-=2;
				StartCoroutine (attackedAnim (0.3f));
				StartCoroutine (canBeAttackedTimer(1));
			}
			
		} 
	}

	IEnumerator attackedAnim(float waitTime) 
	{
		walk = false;
		anim.SetBool ("IsAttacked", true);
		yield return new WaitForSeconds(waitTime);
		anim.SetBool ("IsAttacked", false);
		if (!anim.GetBool ("Charge"))
			walk = true;
	}

	IEnumerator Attacking() 
	{
		anim.SetBool ("Charge", true);
		yield return new WaitForSeconds(1f);

		anim.SetBool ("Attack", true);
		gameObject.collider2D.isTrigger = false;

		yield return new WaitForSeconds(0.5f);
		gameObject.collider2D.isTrigger = true;
		anim.SetBool ("Charge", false);
		anim.SetBool ("Attack", false);
		canAttack = true;
		walk = true;
	}

	void Flip() 
	{
			//Allows the sprite to be facing left or right
			facingRight = !facingRight;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
	}

	IEnumerator spriteSwitch(float waitTime) 
	{
		bluePow.renderer.enabled = true;
		yield return new WaitForSeconds(waitTime);
		bluePow.renderer.enabled = false;
	}

	IEnumerator canBeAttackedTimer(float waitTime) 
	{
		canBeAttacked = false;
		yield return new WaitForSeconds(waitTime);
		canBeAttacked = true;
	}

	
	IEnumerator spriteDie(float waitTime) 
	{
		gameObject.tag = "Untagged";
		walk = false;
		bouncer.collider2D.enabled = false;
		gameObject.collider2D.enabled = false;
		gameObject.collider2D.enabled = false;
		yield return new WaitForSeconds(0.3f);
		anim.SetBool ("Death", true);
		yield return new WaitForSeconds(1f);
		smoke1.particleSystem.Play();
		smoke2.particleSystem.Play();
		gameObject.renderer.enabled = false;
		yield return new WaitForSeconds(waitTime);
		Destroy (gameObject);
		Destroy (bouncer);
	}

	IEnumerator AttackedAnim(float waitTime) 
	{
		walk = false;
		anim.SetBool ("IsAttacked", true);
		yield return new WaitForSeconds(waitTime);
		if (monster2HP > 0)
			anim.SetBool ("IsAttacked", false);
		walk = true;
	}
}


