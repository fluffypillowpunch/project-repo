﻿using UnityEngine;
using System.Collections;

public class Slideshow : MonoBehaviour {

	//Audio
	AudioSource audio;
	AudioSource introaudio;
	AudioSource door;
	AudioSource footsteps;
	AudioSource push;

	//For loading levels
	[SerializeField] private GameObject Introduction;

	//Beginning Legend
	[SerializeField] private GameObject transparent;
	[SerializeField] private GameObject helper;
	[SerializeField] private GameObject text1;
	[SerializeField] private GameObject text2;
	[SerializeField] private GameObject text3;
	[SerializeField] private GameObject text4;
	[SerializeField] private GameObject text5;
	[SerializeField] private GameObject text6;
	[SerializeField] private GameObject text7;
	[SerializeField] private GameObject text8;
	[SerializeField] private GameObject text9;

	//CG Images
	[SerializeField] private GameObject castle;
	[SerializeField] private GameObject CG1;
	[SerializeField] private GameObject CG2;
	[SerializeField] private GameObject CG3;
	[SerializeField] private GameObject CG4;
	[SerializeField] private GameObject CG5;

	//CG Sprites
	[SerializeField] private GameObject lucinormal;
	[SerializeField] private GameObject luciangry;
	[SerializeField] private GameObject satannormal;
	[SerializeField] private GameObject satancry;
	[SerializeField] private GameObject satansmile;
	[SerializeField] private GameObject bubble;

	//CG mini sprites
	[SerializeField] private GameObject lucinormalCG1off;
	[SerializeField] private GameObject lucinormalCG1on;
	[SerializeField] private GameObject luciangryCG1;
	[SerializeField] private GameObject satannormalCG1off;
	[SerializeField] private GameObject satannormalCG1on;
	[SerializeField] private GameObject satancryCG1;
	[SerializeField] private GameObject satansmileCG1;

	//First Text dialogue
	[SerializeField] private GameObject dialoge1p1;
	[SerializeField] private GameObject dialoge1p2;
	[SerializeField] private GameObject dialoge1p3;
	[SerializeField] private GameObject dialoge1p4;
	[SerializeField] private GameObject dialoge1p41;
	[SerializeField] private GameObject dialoge1p5;
	[SerializeField] private GameObject dialoge1p6;
	[SerializeField] private GameObject dialoge1p7;
	[SerializeField] private GameObject dialoge1p71;
	[SerializeField] private GameObject dialoge1p8;
	[SerializeField] private GameObject dialoge1p9;
	[SerializeField] private GameObject dialoge1p91;
	[SerializeField] private GameObject dialoge1p10;
	[SerializeField] private GameObject dialoge1p101;
	[SerializeField] private GameObject dialoge1p11;
	[SerializeField] private GameObject dialoge1p12;
	[SerializeField] private GameObject dialoge1p121;
	[SerializeField] private GameObject dialoge1p13;
	[SerializeField] private GameObject dialoge1p14;
	[SerializeField] private GameObject dialoge1p141;
	[SerializeField] private GameObject dialoge1p15;
	[SerializeField] private GameObject dialoge1p151;
	[SerializeField] private GameObject dialoge1p16;
	[SerializeField] private GameObject dialoge1p161;
	[SerializeField] private GameObject dialoge1p17;
	[SerializeField] private GameObject dialoge1p171;
	[SerializeField] private GameObject dialoge1p18;
	[SerializeField] private GameObject dialoge1p181;
	[SerializeField] private GameObject dialoge1p19;
	//Text dialogue in CG picture 1
	[SerializeField] private GameObject dialoge2p1;
	[SerializeField] private GameObject dialoge2p2;
	[SerializeField] private GameObject dialoge2p3;
	[SerializeField] private GameObject dialoge2p4;
	[SerializeField] private GameObject dialoge2p5;
	[SerializeField] private GameObject dialoge2p6;
	//Text dialogue in CG picture 2
	[SerializeField] private GameObject dialoge3p1;
	[SerializeField] private GameObject dialoge3p2;
	[SerializeField] private GameObject dialoge3p3;
	[SerializeField] private GameObject dialoge3p4;
	//Text dialogue in CG picture 3
	[SerializeField] private GameObject dialoge4p1;
	[SerializeField] private GameObject dialoge4p2;
	[SerializeField] private GameObject dialoge4p3;
	[SerializeField] private GameObject dialoge4p4;
	[SerializeField] private GameObject dialoge4p5;
	//Text dialogue in CG picture 4
	[SerializeField] private GameObject dialoge5p1;
	[SerializeField] private GameObject dialoge5p2;
	[SerializeField] private GameObject dialoge5p3;

	//start from frame 2, since frame 1 is on by default
	bool framet2 = true;
	bool framet3;
	bool framet4;
	bool framet5;
	bool framet6;
	bool framet7;
	bool framet8;
	bool framet9;

	bool dialogueframe1;
	bool dialogueframe2;
	bool dialogueframe3;
	bool dialogueframe4;
	bool dialogueframe5;
	bool dialogueframe6;
	bool dialogueframe7;
	bool dialogueframe8;
	bool dialogueframe9;
	bool dialogueframe10;
	bool dialogueframe11;
	bool dialogueframe12;
	bool dialogueframe13;
	bool dialogueframe14;
	bool dialogueframe15;
	bool dialogueframe16;
	bool dialogueframe17;
	bool dialogueframe18;
	bool dialogueframe19;

	bool CG1frame1;
	bool CG1frame2;
	bool CG1frame3;
	bool CG1frame4;
	bool CG1frame5;
	bool CG1frame6;
	bool CG2frame1;
	bool CG2frame2;
	bool CG2frame3;
	bool CG2frame4;
	bool CG3frame1;
	bool CG3frame2;
	bool CG3frame3;
	bool CG3frame4;
	bool CG4frame1;
	bool CG4frame2;
	bool closeWindow;
    





	// Use this for initialization
	void Start () {
		text1.renderer.enabled = true;
		helper.renderer.enabled = true;

		audio = GetComponent<AudioSource> ();
		introaudio = helper.GetComponent<AudioSource> ();
		door = transparent.GetComponent<AudioSource> ();
		footsteps = castle.GetComponent<AudioSource> ();
		push = bubble.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.UpArrow))
		{
			if (framet2)
			{
				text1.renderer.enabled = false;
				text2.renderer.enabled = true;
				framet2 = false;
				framet3 = true;
			}
			else if (framet3)
			{
				text2.renderer.enabled = false;
				text3.renderer.enabled = true;
				framet3 = false;
				framet4 = true;
			}
			else if (framet4)
			{
				text3.renderer.enabled = false;
				text4.renderer.enabled = true;
				framet4 = false;
				framet5 = true;
			}
			else if (framet5)
			{
				text4.renderer.enabled = false;
				text5.renderer.enabled = true;
				framet5 = false;
				framet6 = true;
			}
			else if (framet6)
			{
				text5.renderer.enabled = false;
				text6.renderer.enabled = true;
				framet6 = false;
				framet7 = true;
			}
			else if (framet7)
			{
				text6.renderer.enabled = false;
				text7.renderer.enabled = true;
				framet7 = false;
				framet8 = true;
			}
			else if (framet8)
			{
				text7.renderer.enabled = false;
				text8.renderer.enabled = true;
				framet8 = false;
				framet9 = true;
			}
			else if (framet9)
			{
				text8.renderer.enabled = false;
				text9.renderer.enabled = true;
				framet9 = false;
				dialogueframe1 = true;
			}
			else if (dialogueframe1)
			{
				dialogueframe1 = false;
				StartCoroutine(pauseframe1 (1f));
			}
			else if (dialogueframe2)
			{
				dialoge1p1.renderer.enabled = false;

				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-2);

				satannormal.renderer.enabled = true;

				dialoge1p2.renderer.enabled = true;

				dialogueframe2 = false;
				dialogueframe3 = true;
		
			}
			else if (dialogueframe3)
			{
				dialoge1p2.renderer.enabled = false;
				
				lucinormal.renderer.enabled = false;
				luciangry.renderer.enabled = true;

				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				
				dialoge1p3.renderer.enabled = true;
				
				dialogueframe3 = false;
				dialogueframe4 = true;
				
			}

			else if (dialogueframe4)
			{
				dialoge1p3.renderer.enabled = false;
				
				lucinormal.renderer.enabled = true;
				luciangry.renderer.enabled = false;

				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-4);
				
				dialoge1p4.renderer.enabled = true;
				dialoge1p41.renderer.enabled = true;
				
				dialogueframe4 = false;
				dialogueframe5 = true;
				
			}

			else if (dialogueframe5)
			{
				dialoge1p4.renderer.enabled = false;
				dialoge1p41.renderer.enabled = false;
				
				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-2);

				satansmile.renderer.enabled = true;

				dialoge1p5.renderer.enabled = true;
				
				dialogueframe5 = false;
				dialogueframe6 = true;
			}
			else if (dialogueframe6)
			{
				dialoge1p5.renderer.enabled = false;
				
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);

				satansmile.renderer.enabled = false;
				
				dialoge1p6.renderer.enabled = true;
				
				dialogueframe6 = false;
				dialogueframe7 = true;
			}
			else if (dialogueframe7)
			{
				dialoge1p6.renderer.enabled = false;
				
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);

				luciangry.renderer.enabled = true;
				
				dialoge1p7.renderer.enabled = true;
				dialoge1p71.renderer.enabled = true;
				
				dialogueframe7 = false;
				dialogueframe8 = true;
			}

			else if (dialogueframe8)
			{
				dialoge1p7.renderer.enabled = false;
				dialoge1p71.renderer.enabled = false;

				luciangry.renderer.enabled = false;
				satansmile.renderer.enabled = true;

				dialoge1p8.renderer.enabled = true;
				
				dialogueframe8 = false;
				dialogueframe9 = true;
			}
			else if (dialogueframe9)
			{
				dialoge1p8.renderer.enabled = false;
				
				dialoge1p9.renderer.enabled = true;
				dialoge1p91.renderer.enabled = true;
				
				dialogueframe9 = false;
				dialogueframe10 = true;
			}
			else if (dialogueframe10)
			{
				dialoge1p9.renderer.enabled = false;
				dialoge1p91.renderer.enabled = false;
				
				dialoge1p10.renderer.enabled = true;
				dialoge1p101.renderer.enabled = true;
				
				dialogueframe10 = false;
				dialogueframe11 = true;
			}
			else if (dialogueframe11)
			{
				dialoge1p10.renderer.enabled = false;
				dialoge1p101.renderer.enabled = false;

				satansmile.renderer.enabled = false;
				luciangry.renderer.enabled = true;

				dialoge1p11.renderer.enabled = true;
				
				dialogueframe11 = false;
				dialogueframe12 = true;
			}
			else if (dialogueframe12)
			{
				dialoge1p11.renderer.enabled = false;
				
				dialoge1p12.renderer.enabled = true;
				dialoge1p121.renderer.enabled = true;
				
				dialogueframe12 = false;
				dialogueframe13 = true;
			}
			else if (dialogueframe13)
			{
				dialoge1p12.renderer.enabled = false;
				dialoge1p121.renderer.enabled = false;

				dialoge1p13.renderer.enabled = true;

				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);
				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-2);
				luciangry.renderer.enabled = false;

				dialogueframe13 = false;
				dialogueframe14 = true;
			}
			else if (dialogueframe14)
			{
				dialoge1p13.renderer.enabled = false;
				
				dialoge1p14.renderer.enabled = true;
				dialoge1p141.renderer.enabled = true;
				
				dialogueframe14 = false;
				dialogueframe15 = true;
			}
			else if (dialogueframe15)
			{
				dialoge1p14.renderer.enabled = false;
				dialoge1p141.renderer.enabled = false;

				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-4);

				dialoge1p15.renderer.enabled = true;
				dialoge1p151.renderer.enabled = true;

				dialogueframe15 = false;
				dialogueframe16 = true;
			}

			else if (dialogueframe16)
			{
				dialoge1p15.renderer.enabled = false;
				dialoge1p151.renderer.enabled = false;

				dialoge1p16.renderer.enabled = true;
				dialoge1p161.renderer.enabled = true;
				
				dialogueframe16 = false;
				dialogueframe17 = true;
			}

			else if (dialogueframe17)
			{
				dialoge1p16.renderer.enabled = false;
				dialoge1p161.renderer.enabled = false;
				
				dialoge1p17.renderer.enabled = true;
				dialoge1p171.renderer.enabled = true;
				
				dialogueframe17 = false;
				dialogueframe18 = true;
			}

			else if (dialogueframe18)
			{
				dialoge1p17.renderer.enabled = false;
				dialoge1p171.renderer.enabled = false;
				
				dialoge1p18.renderer.enabled = true;
				dialoge1p181.renderer.enabled = true;

				satancry.renderer.enabled = true;
				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-2);
				
				dialogueframe18 = false;
				dialogueframe19 = true;
			}

			else if (dialogueframe19)
			{
				dialoge1p18.renderer.enabled = false;
				dialoge1p181.renderer.enabled = false;
				
				dialoge1p19.renderer.enabled = true;
				
				satancry.renderer.enabled = false;
				luciangry.renderer.enabled = true;

				dialogueframe19 = false;
				CG1frame1 = true;
			}	
			else if (CG1frame1)
			{
				CG1frame1 = false;
				StartCoroutine(pauseframeToCG1 (0.5f));
			}
			else if (CG1frame2)
			{
				dialoge2p1.renderer.enabled = false;
				dialoge2p2.renderer.enabled = true;
				
				CG1frame2 = false;
				CG1frame3 = true;
			}
			else if (CG1frame3)
			{
				dialoge2p2.renderer.enabled = false;
				dialoge2p3.renderer.enabled = true;
				
				CG1frame3 = false;
				CG1frame4 = true;
			}
			else if (CG1frame4)
			{
				dialoge2p3.renderer.enabled = false;
				dialoge2p4.renderer.enabled = true;

				satannormalCG1off.renderer.enabled = false;
				satancryCG1.renderer.enabled = true;
				lucinormalCG1off.renderer.enabled = true;
				lucinormalCG1on.renderer.enabled = false;
				
				CG1frame4 = false;
				CG1frame5 = true;
			}
			else if (CG1frame5)
			{
				dialoge2p4.renderer.enabled = false;
				dialoge2p5.renderer.enabled = true;
					
				CG1frame5 = false;
				CG1frame6 = true;
			}
			else if (CG1frame6)
			{
				dialoge2p5.renderer.enabled = false;
				dialoge2p6.renderer.enabled = true;
				
				satannormalCG1off.renderer.enabled = true;
				satancryCG1.renderer.enabled = false;
				lucinormalCG1off.renderer.enabled = false;
				lucinormalCG1on.renderer.enabled = true;
				
				CG1frame6 = false;
				CG2frame1 = true;
			}
			else if (CG2frame1)
			{
				CG2frame1 = false;
				StartCoroutine(pauseframeToCG2 (0.5f));
			}
			else if (CG2frame2)
			{
				dialoge3p1.renderer.enabled = false;

				dialoge3p2.renderer.enabled = true;

				CG2frame2 = false;
				CG2frame3 = true;
			}
			else if (CG2frame3)
			{
				dialoge3p2.renderer.enabled = false;
				
				dialoge3p3.renderer.enabled = true;
				
				CG2frame3 = false;
				CG2frame4 = true;
			}
			else if (CG2frame4)
			{
				dialoge3p3.renderer.enabled = false;
				
				dialoge3p4.renderer.enabled = true;
				
				CG2frame4 = false;
				CG3frame1 = true;
			}
			else if (CG3frame1)
			{
				dialoge3p4.renderer.enabled = false;

				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-4);

				lucinormalCG1off.renderer.enabled = false;
				satannormalCG1on.renderer.enabled = false;
				satancryCG1.renderer.enabled = false;
				CG2.renderer.enabled = false;
				CG1.renderer.enabled = false;
				castle.renderer.enabled = true;

				lucinormal.renderer.enabled = true;
				satannormal.renderer.enabled = true;
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				transparent.renderer.enabled = true;
				bubble.renderer.enabled = true;

				dialoge4p1.renderer.enabled = true;

				CG3frame1 = false;
				CG3frame2 = true;
			}
			else if (CG3frame2)
			{
				CG3frame2 = false;
				StartCoroutine(pauseshowposter(2));
			}
			else if (CG3frame3)
			{
				dialoge4p2.renderer.enabled = false;
				dialoge4p3.renderer.enabled = false;
				dialoge4p4.renderer.enabled = true;

				satannormal.renderer.enabled = false;
				satancry.renderer.enabled = true;
				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-2);

				CG3frame3 = false;
				CG3frame4 = true;
			}
			else if (CG3frame4)
			{
				dialoge4p4.renderer.enabled = false;
				dialoge4p5.renderer.enabled = true;

				//push
				push.Play ();
				satannormal.renderer.enabled = true;
				satancry.renderer.enabled = false;
				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-4);
				
				CG3frame4 = false;
				CG4frame1 = true;
			}
			else if (CG4frame1)
			{
				CG4frame1 = false;
				StartCoroutine(pauseFalling(0.5f));
			}
			else if (CG4frame2)
			{
				dialoge5p1.renderer.enabled = false;
				dialoge5p2.renderer.enabled = true;

				CG4frame2 = false;
				closeWindow = true;

			}
			else if (closeWindow)
			{
				Destroy(Introduction);
				GameObject MainLevels = Instantiate(Resources.Load("Main Levels")) as GameObject;
			}

		}
	
	}

	IEnumerator pauseframe1(float waitTime) 
	{
		footsteps.Play ();
		introaudio.Stop ();
		helper.renderer.enabled = false;
		text9.renderer.enabled = false;
		castle.renderer.enabled = true;
		yield return new WaitForSeconds(1f);
		door.Play ();
		yield return new WaitForSeconds(1f);
		audio.Play ();
		yield return new WaitForSeconds(waitTime);
		transparent.renderer.enabled = true;
		lucinormal.renderer.enabled = true;
		dialoge1p1.renderer.enabled = true;
		bubble.renderer.enabled = true;
		dialogueframe2 = true;
	}
	IEnumerator pauseframeToCG1(float waitTime) 
	{
		dialoge1p19.renderer.enabled = false;
		satancry.renderer.enabled = false;
		luciangry.renderer.enabled = false;
		satannormal.renderer.enabled = false;
		lucinormal.renderer.enabled = false;
		bubble.renderer.enabled = false;
		transparent.renderer.enabled = false;
		CG1.renderer.enabled = true;
		yield return new WaitForSeconds(waitTime);
		lucinormalCG1on.renderer.enabled = true;
		satannormalCG1off.renderer.enabled = true;
		dialoge2p1.renderer.enabled = true;
	
		CG1frame2 = true;
	}
	IEnumerator pauseframeToCG2(float waitTime) 
	{
		dialoge2p6.renderer.enabled = false;
		
		CG2.renderer.enabled = true;
		yield return new WaitForSeconds(waitTime);
		satannormalCG1off.renderer.enabled = false;
		satancryCG1.renderer.enabled = true;
		lucinormalCG1off.renderer.enabled = true;
		lucinormalCG1on.renderer.enabled = false;
		
		dialoge3p1.renderer.enabled = true;

		CG2frame2 = true;
	}
	IEnumerator pauseshowposter(float waitTime) 
	{
		lucinormal.renderer.enabled = false;
		satannormal.renderer.enabled = false;
		transparent.renderer.enabled = false;
		bubble.renderer.enabled = false;
		dialoge4p1.renderer.enabled = false;
		CG3.renderer.enabled = true;
		yield return new WaitForSeconds(waitTime);

		CG3.renderer.enabled = false;
		lucinormal.renderer.enabled = true;
		satannormal.renderer.enabled = true;
		transparent.renderer.enabled = true;
		bubble.renderer.enabled = true;
		dialoge4p2.renderer.enabled = true;
		dialoge4p3.renderer.enabled = true;

		CG3frame3 = true;
	}
	IEnumerator pauseFalling(float waitTime) 
	{
		dialoge4p5.renderer.enabled = false;
		satannormal.renderer.enabled = false;
		lucinormal.renderer.enabled = false;
		castle.renderer.enabled = false;
		CG4.renderer.enabled = true;
		transparent.renderer.enabled = false;
		bubble.renderer.enabled = false;
		yield return new WaitForSeconds(waitTime);
		dialoge5p1.renderer.enabled = true;

		CG4frame2 = true;
	}
}
