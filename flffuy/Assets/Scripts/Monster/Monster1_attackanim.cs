﻿using UnityEngine;
using System.Collections;

public class Monster1_attackanim : MonoBehaviour {
	
	Animator anim;

	float monster1HP = 1;
	bool canBeAttacked = true;
	public GameObject bluePow;
	[SerializeField] private GameObject smoke;
	[SerializeField] private GameObject bounce;
	AudioSource pillowThrow;
	AudioSource pillowHit;


	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		pillowThrow = GetComponent<AudioSource> ();
		pillowHit = bounce.GetComponent<AudioSource> ();

	}
	
	// Update is called once per frame
	void Update () {

		if (monster1HP <= 0) 
		{
			StartCoroutine (spriteDie (0.5f));
		}
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.gameObject.tag == "PillowAttackingCollider")
		{
			//Trigger being hit
			if (monster1HP > 0 && canBeAttacked) 
			{
				monster1HP--;
				StartCoroutine (spriteSwitch (0.2f));
				StartCoroutine (AttackedAnim (0.2f));
				pillowHit.Play();
			} 

		}

		if (other.gameObject.tag == "JumpAttackingCollider")
		{
			//Trigger being hit
			if (monster1HP > 0 && canBeAttacked) 
			{
				monster1HP--;
				StartCoroutine (jumpSwitch (0.2f));
				StartCoroutine (AttackedAnim (0.2f));
			} 
		}

		if (other.gameObject.tag == "Pillow")
		{
			//Trigger being hit
			if (monster1HP > 0 && canBeAttacked) 
			{
				pillowThrow.Play();
				monster1HP--;
				StartCoroutine (jumpSwitch (0.2f));
				StartCoroutine (AttackedAnim (0.2f));
			} 
		}
	}

	IEnumerator AttackedAnim(float waitTime) 
	{
		anim.SetBool ("IsAttacked", true);
		yield return new WaitForSeconds(waitTime);
		if (monster1HP > 0)
			anim.SetBool ("IsAttacked", false);
	}

	IEnumerator spriteSwitch(float waitTime) 
	{
		canBeAttacked = false;
		bluePow.renderer.enabled = true;
		yield return new WaitForSeconds(waitTime);
		bluePow.renderer.enabled = false;
		canBeAttacked = true;
	}

	IEnumerator jumpSwitch(float waitTime) 
	{
		canBeAttacked = false;
		yield return new WaitForSeconds(waitTime);
		canBeAttacked = true;
	}
	
	IEnumerator spriteDie(float waitTime) 
	{
		gameObject.tag = "Untagged";
		gameObject.collider2D.enabled = false;
		anim.SetBool ("IsAttacked", true);
		yield return new WaitForSeconds(0.2f);
		smoke.particleSystem.Play();
		gameObject.renderer.enabled = false;
		yield return new WaitForSeconds(waitTime);
		Destroy (gameObject);
	}



}
