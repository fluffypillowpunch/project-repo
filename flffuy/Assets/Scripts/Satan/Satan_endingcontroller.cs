﻿using UnityEngine;
using System.Collections;

public class Satan_endingcontroller : MonoBehaviour {

	//THIS IS A SIMPLIFIED VERSION OF THE Satan_controller_script.cs used only for the
	//ending scene!




	//Movement and animation variables
	public float maxSpeed = 10f;
	bool facingRight = true;
	Animator anim;
	bool canRun = true;
	int satanHP;
	
	//Jump Variables and ground check
	bool doubleJump = false;
	public float jumpForce = 1000f;
	bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.3f;
	public LayerMask whatIsGround;
	
	//Attack Variables
	bool attack;
	bool canAttack;

	void Start () {
		
		anim = GetComponent<Animator> ();  //Combine the animator component to the scrip
	}
	
	void FixedUpdate () 
	{
			//Checks for ground: is there a collider there?
			grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
			anim.SetBool ("Ground", grounded);
			
			if (grounded) {
				doubleJump = false;
				canAttack = true;
			} else 
				canAttack = false;
			
			//MOVING
			float move = Input.GetAxis ("Horizontal");		//Set left (-1) and right (1)
			
			if (canRun == true) {
				anim.SetFloat ("vSpeed", rigidbody2D.velocity.y); // Set animations for jumping
				anim.SetFloat ("Speed", Mathf.Abs (move));		//Set animations for running
				rigidbody2D.velocity = new Vector2 (move * maxSpeed, rigidbody2D.velocity.y);	//Move player left and right
			}

			
			//Correctly orientate the player (facing left or right?)
			if (move > 0 && !facingRight)
				Flip ();
			else if (move < 0 && facingRight)
				Flip ();
			
			
			//MELEE ATTACK
			attack = Input.GetKeyDown (KeyCode.Z);
			if (canAttack) {
				if (attack && move != 0) {
					rigidbody2D.velocity = new Vector2 (0.11f, 0);
					anim.SetBool ("Attack", true);
					StartCoroutine (attackPause (0.15f));
				} else if (attack) {
					anim.SetBool ("Attack", true);
				} else {
					anim.SetBool ("Attack", false);    //stop attack animation (prevent loop)
				}	
			}
			

	}
	
	
	void Update()
	{	
		
		if ((grounded || !doubleJump) && Input.GetKeyDown (KeyCode.Space)) 
		{
			anim.SetBool ("Ground", false); //Set animations for jumping
			
			if (!doubleJump && !grounded)  //Double Jumping
			{	   
				doubleJump = true; 									//Prevent another jump
				rigidbody2D.velocity = new Vector2 (0, 0); 			//Reset velocity for even jumping
				rigidbody2D.AddForce (new Vector2 (0,jumpForce));	//perform double jump
			}
			else
				rigidbody2D.AddForce (new Vector2 (0, jumpForce)); //do regular jump
		}
				
	}
	
	void Flip() 
	{
			//Allows the sprite to be facing left or right
			facingRight = !facingRight;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
	}

	
	IEnumerator attackPause(float waitTime) 
	{
		canRun = false;
		yield return new WaitForSeconds(waitTime);
		canRun = true;
	}
	
}

//REFERENCES
// 1. [Unity Tutorial] Platform | Sidescroller 01   - Youtube Video
// Author: Sebastian Lague
// Date accessed: 13th April 2014
// URL: https://www.youtube.com/watch?v=d3HEFiDFApI

// 2. 2d Character Controllers (Unity Tutorial)
// Author: Unity      Tutor: Mike Geig
// Date accessed: 12th April 2014
// http://unity3d.com/learn/tutorials/modules/beginner/2d/2d-controllers
// Used for movement, flipping of sprites, animation, Jumping

// 3. [Unity Tutorial] Platform | Sidescroller 04   - Youtube Video
// Author: Sebastian Lague
// Date accessed: 17th April 2014
// URL: https://www.youtube.com/watch?v=zirvkEWxPXU

//4. Throwing pillow - Instantiate
//https://docs.unity3d.com/Documentation/ScriptReference/Object.Instantiate.html
//25/4





