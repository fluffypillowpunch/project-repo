﻿using UnityEngine;
using System.Collections;

public class MinibossText : MonoBehaviour {
	
	//Bubble
	[SerializeField] private GameObject bubble;
	
	//Text Lines
	[SerializeField] private GameObject text1;
	[SerializeField] private GameObject text2;
	[SerializeField] private GameObject text21;
	[SerializeField] private GameObject text3;
	[SerializeField] private GameObject text31;
	[SerializeField] private GameObject text4;
	[SerializeField] private GameObject text5;
	[SerializeField] private GameObject text51;
	[SerializeField] private GameObject text6;
	[SerializeField] private GameObject text7;
	[SerializeField] private GameObject text71;
	
	
	//Scene elements
	[SerializeField] private GameObject player;
	[SerializeField] private GameObject minitalk;
	[SerializeField] private GameObject miniblack;
	[SerializeField] private GameObject satancry;
	[SerializeField] private GameObject satannormal;
	[SerializeField] private GameObject satansmile;
	[SerializeField] private GameObject Dim;
	
	//Text Orders
	bool isEnabled;
	bool frame2;
	bool frame3;
	bool frame4;
	bool frame5;
	bool frame6;
	bool frame7;
	bool exit;
	
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
		if (isEnabled) 
		{
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				if (frame2)
				{
					//deactivate frame 1
					text1.gameObject.renderer.enabled = false;
					
					satansmile.renderer.enabled = true;
					satansmile.transform.position = new Vector3 (satansmile.transform.position.x, satansmile.transform.position.y,-5);
					minitalk.renderer.enabled = false;
					miniblack.renderer.enabled = true;
					
					//activate frame 2
					text2.gameObject.renderer.enabled = true;
					text21.gameObject.renderer.enabled = true;
					
					frame2 = false;
					frame3 = true;
				}
				else if (frame3)
				{
					//deactivate frame 1
					text2.gameObject.renderer.enabled = false;
					text21.gameObject.renderer.enabled = false;
					
					
					satansmile.renderer.enabled = false;
					satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-1);
					
					minitalk.renderer.enabled = true;
					miniblack.renderer.enabled = false;
					
					//activate frame 2
					text3.gameObject.renderer.enabled = true;
					text31.gameObject.renderer.enabled = true;
					
					frame3 = false;
					frame4 = true;
				}
				else if (frame4)
				{
					//deactivate frame 1
					text3.gameObject.renderer.enabled = false;
					text31.gameObject.renderer.enabled = false;
					
					satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-5);
					
					minitalk.renderer.enabled = false;
					miniblack.renderer.enabled = true;
					
					//activate frame 2
					text4.gameObject.renderer.enabled = true;
					
					frame4 = false;
					frame5 = true;
				}
				else if (frame5)
				{
					//deactivate frame 1
					text4.gameObject.renderer.enabled = false;
					
					satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-1);
					
					minitalk.renderer.enabled = true;
					miniblack.renderer.enabled = false;
					
					//activate frame 2
					text5.gameObject.renderer.enabled = true;
					text51.gameObject.renderer.enabled = true;
					
					frame5 = false;
					frame6 = true;
				}
				else if (frame6)
				{
					//deactivate frame 1
					text5.gameObject.renderer.enabled = false;
					text51.gameObject.renderer.enabled = false;
					
					satancry.renderer.enabled = true;
					satancry.transform.position = new Vector3 (satancry.transform.position.x, satancry.transform.position.y,-5);
					
					minitalk.renderer.enabled = false;
					miniblack.renderer.enabled = true;
					
					//activate frame 2
					text6.gameObject.renderer.enabled = true;
					
					frame6 = false;
					frame7 = true;
				}
				else if (frame7)
				{
					//deactivate frame 1
					text6.gameObject.renderer.enabled = false;
					
					//activate frame 2
					text7.gameObject.renderer.enabled = true;
					text71.gameObject.renderer.enabled = true;
					
					frame7 = false;
					exit = true;
				}
				else if (exit)
				{
					//deactivate frame 2
					text7.gameObject.renderer.enabled = false;
					text71.gameObject.renderer.enabled = false;

					//close bubble
					bubble.gameObject.renderer.enabled = false;
					
					//disable script
					isEnabled = false;
					
					//close minister
					satannormal.renderer.enabled = false;
					minitalk.renderer.enabled = false;
					miniblack.renderer.enabled = false;
					satancry.renderer.enabled = false;
					
					Dim.gameObject.renderer.enabled = false;
					
					//resume time and prevent superjumping
					Time.timeScale = 1;
					player.rigidbody2D.isKinematic = false;
					
				}
			}
		}
		
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			
			//pause time and prevent superjumping
			Time.timeScale = 0;
			other.rigidbody2D.isKinematic = true;
			
			Dim.gameObject.renderer.enabled = true;
			satannormal.renderer.enabled = true;
			minitalk.renderer.enabled = true;
			minitalk.transform.position = new Vector3 (miniblack.transform.position.x, miniblack.transform.position.y,-5);
			miniblack.transform.position = new Vector3 (miniblack.transform.position.x, miniblack.transform.position.y,-5);
			
			satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-1);
			
			//frame 1
			bubble.gameObject.renderer.enabled = true;
			text1.gameObject.renderer.enabled = true;
			
			Destroy(gameObject.collider2D);
			isEnabled = true;
			
			frame2 = true; //play second frame
		}
	}
	
}



