﻿using UnityEngine;
using System.Collections;

public class EndingPart2 : MonoBehaviour {

	[SerializeField] private GameObject camera;

	AudioSource endingpt1;
	[SerializeField] private GameObject endingobject;

	AudioSource pew;
	[SerializeField] private GameObject transparent;

	AudioSource heal;
	[SerializeField] private GameObject healObject;

	AudioSource dungeon;
	[SerializeField] private GameObject dungeonObject;

	AudioSource goofy;
	[SerializeField] private GameObject goofyObject;
	
	AudioSource church;
	[SerializeField] private GameObject churchObject;

	[SerializeField] private GameObject black;
	//CG Sprites
	[SerializeField] private GameObject lucinormal;
	[SerializeField] private GameObject luciangry;
	[SerializeField] private GameObject heronormal;
	[SerializeField] private GameObject herosmile;
	[SerializeField] private GameObject heroblack;
	[SerializeField] private GameObject satannormal;
	[SerializeField] private GameObject satancry;
	[SerializeField] private GameObject satansmile;
	[SerializeField] private GameObject bubble;
	[SerializeField] private GameObject CG1;
	
	//First Text dialogue
	[SerializeField] private GameObject dialogue1p1;
	[SerializeField] private GameObject dialogue1p11;
	[SerializeField] private GameObject dialogue1p2;
	[SerializeField] private GameObject dialogue1p3;
	[SerializeField] private GameObject dialogue1p4;
	[SerializeField] private GameObject dialogue1p5;
	[SerializeField] private GameObject dialogue1p51;
	[SerializeField] private GameObject dialogue1p6;
	[SerializeField] private GameObject dialogue1p61;
	[SerializeField] private GameObject dialogue1p7;
	[SerializeField] private GameObject dialogue1p8;
	[SerializeField] private GameObject dialogue1p9;
	[SerializeField] private GameObject dialogue1p10;
	[SerializeField] private GameObject dialogue1p111;
	[SerializeField] private GameObject dialogue1p12;
	[SerializeField] private GameObject dialogue1p121;
	[SerializeField] private GameObject dialogue1p13;
	[SerializeField] private GameObject dialogue1p131;
	[SerializeField] private GameObject dialogue1p14;
	[SerializeField] private GameObject dialogue1p141;
	[SerializeField] private GameObject dialogue1p15;
	[SerializeField] private GameObject dialogue1p151;
	[SerializeField] private GameObject dialogue1p16;
	[SerializeField] private GameObject dialogue1p161;
	[SerializeField] private GameObject dialogue1p17;
	[SerializeField] private GameObject dialogue1p171;
	[SerializeField] private GameObject dialogue1p172;
	[SerializeField] private GameObject dialogue1p18;
	[SerializeField] private GameObject dialogue1p181;
	[SerializeField] private GameObject dialogue1p19;
	[SerializeField] private GameObject dialogue1p191;
	[SerializeField] private GameObject CGText1;
	[SerializeField] private GameObject CGText2;
	[SerializeField] private GameObject CGText3;
	[SerializeField] private GameObject EndText4;
	[SerializeField] private GameObject EndText5;
	[SerializeField] private GameObject EndText6;
	[SerializeField] private GameObject EndText7;
	[SerializeField] private GameObject EndText71;
	[SerializeField] private GameObject EndText8;
	[SerializeField] private GameObject EndText81;
	[SerializeField] private GameObject EndText9;
	[SerializeField] private GameObject EndText10;

	bool dialogueframe2;
	bool dialogueframe3;
	bool dialogueframe4;
	bool dialogueframe5;
	bool dialogueframe6;
	bool dialogueframe7;
	bool dialogueframe8;
	bool dialogueframe9;
	bool dialogueframe10;
	bool dialogueframe11;
	bool dialogueframe12;
	bool dialogueframe13;
	bool dialogueframe14;
	bool dialogueframe15;
	bool dialogueframe16;
	bool dialogueframe17;
	bool dialogueframe18;
	bool dialogueframe19;
	bool dialogueframe20;
	bool dialogueframe21;
	bool dialogueframe22;
	bool dialogueframe23;
	bool dialogueframe24;
	bool dialogueframe25;
	bool dialogueframe26;
	bool dialogueframe27;
	bool dialogueframe28;
	bool dialogueframe29;
	bool closewindow;
	bool canRestart;

	// Use this for initialization
	void Start () {

		endingpt1 = endingobject.GetComponent<AudioSource> ();
		pew = transparent.GetComponent<AudioSource> ();
		heal = healObject.GetComponent<AudioSource> ();
		dungeon = dungeonObject.GetComponent<AudioSource> ();
		goofy = goofyObject.GetComponent<AudioSource> ();
		church = churchObject.GetComponent<AudioSource> ();
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.UpArrow))
		{
			if (dialogueframe2)
			{
				//Turn off dialogue
				dialogue1p1.renderer.enabled = false;
				dialogue1p11.renderer.enabled = false;
				
				satansmile.renderer.enabled = true;
				
				//Turn on Dialogue
				dialogue1p2.renderer.enabled = true;
				heroblack.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-2);

				
				//Set to next frame
				dialogueframe2 = false;
				dialogueframe3 = true;
			}
			else if (dialogueframe3)
			{
				//Turn off dialogue
				dialogue1p2.renderer.enabled = false;

				satansmile.renderer.enabled = false;
				heroblack.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-4);
				heal.Play();
				
				//Turn on Dialogue
				dialogue1p3.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe3 = false;
				dialogueframe4 = true;
			}
			else if (dialogueframe4)
			{
				//Turn off dialogue
				dialogue1p3.renderer.enabled = false;

				heroblack.renderer.enabled = false;
				lucinormal.renderer.enabled = true;

				//Turn on Dialogue
				dialogue1p4.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe4 = false;
				dialogueframe5 = true;
			}
			else if (dialogueframe5)
			{
				//Turn off dialogue
				dialogue1p4.renderer.enabled = false;
				
				//Turn on Dialogue
				dialogue1p5.renderer.enabled = true;
				dialogue1p51.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe5 = false;
				dialogueframe6 = true;
			}
			else if (dialogueframe6)
			{
				//Turn off dialogue
				dialogue1p5.renderer.enabled = false;
				dialogue1p51.renderer.enabled = false;
				
				//Turn on Dialogue
				dialogue1p6.renderer.enabled = true;
				dialogue1p61.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe6 = false;
				dialogueframe7 = true;
			}
			else if (dialogueframe7)
			{
				//Turn off dialogue
				dialogue1p6.renderer.enabled = false;
				dialogue1p61.renderer.enabled = false;

				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-2);
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);

				//Turn on Dialogue
				dialogue1p7.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe7 = false;
				dialogueframe8 = true;
			}
			else if (dialogueframe8)
			{
				//Turn off dialogue
				dialogue1p7.renderer.enabled = false;
				
				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-4);
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				
				//Turn on Dialogue
				dialogue1p8.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe8 = false;
				dialogueframe9 = true;
			}
			else if (dialogueframe9)
			{
				//Turn off dialogue
				dialogue1p8.renderer.enabled = false;
				
				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-2);
				satansmile.renderer.enabled = true;
				//Turn on Dialogue
				dialogue1p9.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe9 = false;
				dialogueframe10 = true;
			}
			else if (dialogueframe10)
			{
				//Turn off dialogue
				dialogue1p9.renderer.enabled = false;

				satansmile.renderer.enabled = false;
				lucinormal.renderer.enabled = false;
				heronormal.renderer.enabled = true;
				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-4);
				dungeon.Play ();

				//Turn on Dialogue
				dialogue1p10.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe10 = false;
				dialogueframe11 = true;
			}
			else if (dialogueframe11)
			{
				//Turn off dialogue
				dialogue1p10.renderer.enabled = false;

				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-2);

				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);

				//Turn on Dialogue
				dialogue1p111.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe11 = false;
				dialogueframe12 = true;
			}
			else if (dialogueframe12)
			{
				//Turn off dialogue
				dialogue1p111.renderer.enabled = false;
				
				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-4);
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				
				//Turn on Dialogue
				dialogue1p12.renderer.enabled = true;
				dialogue1p121.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe12 = false;
				dialogueframe13 = true;
			}
			else if (dialogueframe13)
			{
				//Turn off dialogue
				dialogue1p12.renderer.enabled = false;
				dialogue1p121.renderer.enabled = false;

				//Turn on Dialogue
				dialogue1p13.renderer.enabled = true;
				dialogue1p131.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe13 = false;
				dialogueframe14 = true;
			}
			else if (dialogueframe14)
			{
				//Turn off dialogue
				dialogue1p13.renderer.enabled = false;
				dialogue1p131.renderer.enabled = false;

				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-2);
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);

				//Turn on Dialogue
				dialogue1p14.renderer.enabled = true;
				dialogue1p141.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe14 = false;
				dialogueframe15 = true;
			}
			else if (dialogueframe15)
			{
				//Turn off dialogue
				dialogue1p14.renderer.enabled = false;
				dialogue1p141.renderer.enabled = false;
				
				herosmile.renderer.enabled = true;
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				
				//Turn on Dialogue
				dialogue1p15.renderer.enabled = true;
				dialogue1p151.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe15 = false;
				dialogueframe16 = true;
			}
			else if (dialogueframe16)
			{
				//Turn off dialogue
				dialogue1p15.renderer.enabled = false;
				dialogue1p151.renderer.enabled = false;
				
				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-4);
				herosmile.renderer.enabled = false;

				//Turn on Dialogue
				dialogue1p16.renderer.enabled = true;
				dialogue1p161.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe16 = false;
				dialogueframe17 = true;
			}
			else if (dialogueframe17)
			{
				//Turn off dialogue
				dialogue1p16.renderer.enabled = false;
				dialogue1p161.renderer.enabled = false;
				
				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-2);
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);


				//Turn on Dialogue
				dialogue1p17.renderer.enabled = true;
				dialogue1p171.renderer.enabled = true;
				dialogue1p172.renderer.enabled = true;

				//Set to next frame
				dialogueframe17 = false;
				dialogueframe18 = true;
			}
			else if (dialogueframe18)
			{
				//Turn off dialogue
				dialogue1p17.renderer.enabled = false;
				dialogue1p171.renderer.enabled = false;
				dialogue1p172.renderer.enabled = false;
				
				//Turn on Dialogue
				dialogue1p18.renderer.enabled = true;
				dialogue1p181.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe18 = false;
				dialogueframe19 = true;
			}
			else if (dialogueframe19)
			{
				//Turn off dialogue
				dialogue1p18.renderer.enabled = false;
				dialogue1p181.renderer.enabled = false;

				herosmile.renderer.enabled = true;
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				goofy.Play ();
				dungeon.Stop ();

				//Turn on Dialogue
				dialogue1p19.renderer.enabled = true;
				dialogue1p191.renderer.enabled = true;

				//Set to next frame
				dialogueframe19 = false;
				dialogueframe20 = true;
			}
			else if (dialogueframe20)
			{
				//Turn off dialogue
				dialogue1p19.renderer.enabled = false;
				dialogue1p191.renderer.enabled = false;

				heronormal.renderer.enabled = false;
				herosmile.renderer.enabled = false;
				satannormal.renderer.enabled = false;
				transparent.renderer.enabled = false;
				bubble.renderer.enabled = false;

				CG1.renderer.enabled = true;
				CGText1.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe20 = false;
				dialogueframe21 = true;
			}
			else if (dialogueframe21)
			{
				CGText1.renderer.enabled = false;
				CGText2.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe21 = false;
				dialogueframe22 = true;
			}
			else if (dialogueframe22)
			{
				CGText2.renderer.enabled = false;
				CGText3.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe22 = false;
				dialogueframe23 = true;
			}
			else if (dialogueframe23)
			{
				CG1.renderer.enabled = false;
				CGText3.renderer.enabled = false;

				transparent.renderer.enabled = true;
				bubble.renderer.enabled = true;
				lucinormal.renderer.enabled = true;
				satannormal.renderer.enabled = true;

				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-4);
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);

				EndText4.renderer.enabled = true;

				//Set to next frame
				dialogueframe23 = false;
				dialogueframe24 = true;
			}
			else if (dialogueframe24)
			{
				EndText4.renderer.enabled = false;
			
				
				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-2);
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);
				
				EndText5.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe24 = false;
				dialogueframe25 = true;
			}
			else if (dialogueframe25)
			{
				EndText5.renderer.enabled = false;	
				
				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-4);
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				
				EndText6.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe25 = false;
				dialogueframe26 = true;
			}
			else if (dialogueframe26)
			{
				EndText6.renderer.enabled = false;	

				lucinormal.renderer.enabled = false;
				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-4);
				heronormal.renderer.enabled = true;
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				
				EndText7.renderer.enabled = true;
				EndText71.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe26 = false;
				dialogueframe27 = true;
			}
			else if (dialogueframe27)
			{
				EndText7.renderer.enabled = false;	
				EndText71.renderer.enabled = false;
				
				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-2);
				heronormal.renderer.enabled = true;
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);
				
				EndText8.renderer.enabled = true;
				EndText81.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe27 = false;
				dialogueframe28 = true;
			}
			else if (dialogueframe28)
			{
				EndText8.renderer.enabled = false;	
				EndText81.renderer.enabled = false;
				black.renderer.enabled = true;
				lucinormal.renderer.enabled = true;
				lucinormal.transform.position = new Vector3 (lucinormal.transform.position.x, lucinormal.transform.position.y,-9);
				heronormal.renderer.enabled = false;
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				church.Play();
				church.volume = 0.6f;
				goofy.Stop();

				EndText9.renderer.enabled = true;

				//Set to next frame
				dialogueframe28 = false;
				dialogueframe29 = true;
			}
			else if (dialogueframe29)
			{
				EndText10.renderer.enabled = true;

				//Set to next frame
				dialogueframe29 = false;
				closewindow = true;
			}
			else if (closewindow)
			{
				heroblack.renderer.enabled = false;
				transparent.renderer.enabled = false;
				bubble.renderer.enabled = false;
				this.collider2D.enabled = false;
				Time.timeScale = 1;

				//Show CG
				camera.transform.position = new Vector3(-24,-2,-10);
				canRestart = true;
			}

		}

		if (Input.GetKeyDown (KeyCode.R))
		{
			Application.LoadLevel("Title Screen");
		}

        }



	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "PillowAttackingCollider")
		{
			pew.Play();
			endingpt1.Stop();

			Time.timeScale = 0;
			transparent.renderer.enabled = true;
			bubble.renderer.enabled = true;
			dialogue1p1.renderer.enabled =true;
			dialogue1p11.renderer.enabled =true;

			satannormal.renderer.enabled = true;
			satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);

			heroblack.renderer.enabled = true;
			heroblack.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-4);

			dialogueframe2 = true;

		}
	}

	void fadeIn(AudioSource audio)
	{
		float volume = 0;

		while (volume < 1)
		{
			volume += Time.deltaTime;
			audio.volume = volume;
		}
	}

	void fadeOut(AudioSource audio)
	{
		while (audio.volume > 0) 
		{
			audio.volume -= Time.deltaTime;
		}
	}
}
