﻿using UnityEngine;
using System.Collections;

		// REFERENCE
		// This script was created under the guidance (and tutorial) of the 2D Tower Platformer Demo made by Unity.
		// Game Source : Unity Platform Demo   Publisher: Unity Technologies   Script: CameraFollow.cs
		// Date accessed: Monday 16th April
		// URL: https://www.assetstore.unity3d.com/#/content/11228
 

public class Camera_follow : MonoBehaviour 
{
	public float xBoundry = 1f;		
	public float yBoundry = 1f;		
	public float xLerpAmt = 5f;	
	public float yLerpAmt = 5f;		
	public Vector2 maxCameraXY;		// how far the camera can go in x and y axis
	public Vector2 minCameraXY;		
	
	private Transform player;
	
	void Awake () 		// Find Satan and get transform
	{
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	bool CheckXMargin()   //Check if player moves left or right
	{
		return Mathf.Abs(transform.position.x - player.position.x) > xBoundry;
	}

	bool CheckYMargin()   //Check if player moves up or down
	{
		return Mathf.Abs(transform.position.y - player.position.y) > yBoundry;
	}

	void LateUpdate ()
	{
		TrackPlayer();
	}

	void TrackPlayer ()
	{
		// Initialise camera transforms
		float targetX = transform.position.x;
		float targetY = transform.position.y;

		//If satan moves along x axis, the camera will lerp to player's x position
		if (CheckXMargin ()) 
		{
			targetX = Mathf.Lerp (transform.position.x, player.position.x, xLerpAmt * Time.deltaTime);
		}

		//If satan moves along y axis, the camera will lerp to player's y position
		if (CheckYMargin ()) 
		{
			targetY = Mathf.Lerp (transform.position.y, player.position.y-0.2f, yLerpAmt * Time.deltaTime);
		}

		// Keep the values of x and y in the max and minimum
		targetX = Mathf.Clamp(targetX, minCameraXY.x, maxCameraXY.x);
		targetY = Mathf.Clamp(targetY, minCameraXY.y, maxCameraXY.y);

		transform.position = new Vector3(targetX, targetY, transform.position.z);
	}
}
