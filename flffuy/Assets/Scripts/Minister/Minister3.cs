﻿using UnityEngine;
using System.Collections;

public class Minister3 : MonoBehaviour {

	//particles
	[SerializeField] private GameObject smoke;

	//Animator
	Animator anim;
	
	//Bubble
	[SerializeField] private GameObject bubble;

	//Text lines
	[SerializeField] private GameObject text1;
	[SerializeField] private GameObject text11;
	[SerializeField] private GameObject text2;
	[SerializeField] private GameObject text21;
	[SerializeField] private GameObject text22;
	[SerializeField] private GameObject text3;
	[SerializeField] private GameObject text31;
	[SerializeField] private GameObject text4;
	[SerializeField] private GameObject text5;
	[SerializeField] private GameObject text6;
	[SerializeField] private GameObject text61;
	[SerializeField] private GameObject text62;
	
	//Scene elements
	[SerializeField] private GameObject player;
	[SerializeField] private GameObject MinisterNormal;
	[SerializeField] private GameObject MinisterAngry;
	[SerializeField] private GameObject satancry;
	[SerializeField] private GameObject satannormal;
	[SerializeField] private GameObject satansmile;
	[SerializeField] private GameObject Dim;
	
	//Text Orders
	bool isEnabled;
	bool frame2;
	bool frame3;
	bool frame4;
	bool frame5;
	bool frame6;
	bool exit;
	
	
	// Use this for initialization
	void Start () {
		
		anim = GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
		
		if (isEnabled) 
		{
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				if (frame2)
				{
					//deactivate frame 1
					text1.gameObject.renderer.enabled = false;
					text11.gameObject.renderer.enabled = false;

					satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-5);
					MinisterNormal.transform.position = new Vector3 (MinisterNormal.transform.position.x, MinisterNormal.transform.position.y,-1);

					//activate frame 2
					text2.gameObject.renderer.enabled = true;
					text21.gameObject.renderer.enabled = true;
					text22.gameObject.renderer.enabled = true;
					
					frame2 = false;
					frame3 = true;
				}
				else if (frame3)
				{
					//deactivate frame 1
					text2.gameObject.renderer.enabled = false;
					text21.gameObject.renderer.enabled = false;
					text22.gameObject.renderer.enabled = false;
					
					satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-1);
					MinisterNormal.transform.position = new Vector3 (MinisterNormal.transform.position.x, MinisterNormal.transform.position.y,-5);
					
					//activate frame 2
					text3.gameObject.renderer.enabled = true;
					text31.gameObject.renderer.enabled = true;

					frame3 = false;
					frame4 = true;
				}
				else if (frame4)
				{
					//deactivate frame 1
					text3.gameObject.renderer.enabled = false;
					text31.gameObject.renderer.enabled = false;

					//activate frame 2
					text4.gameObject.renderer.enabled = true;

					frame4 = false;
					frame5 = true;
				}
				else if (frame5)
				{
					//deactivate frame 1
					text4.gameObject.renderer.enabled = false;

					satancry.renderer.enabled = true;
					satancry.transform.position = new Vector3 (satancry.transform.position.x, satancry.transform.position.y,-5);

					MinisterNormal.transform.position = new Vector3 (MinisterNormal.transform.position.x, MinisterNormal.transform.position.y,-1);

					//activate frame 2
					text5.gameObject.renderer.enabled = true;

					frame5 = false;
					frame6 = true;
				}
				else if (frame6)
				{
					//deactivate frame 1
					text5.gameObject.renderer.enabled = false;

					satancry.renderer.enabled = false;
					MinisterNormal.transform.position = new Vector3 (MinisterNormal.transform.position.x, MinisterNormal.transform.position.y,-5);
					
					//activate frame 2
					text6.gameObject.renderer.enabled = true;
					text61.gameObject.renderer.enabled = true;
					text62.gameObject.renderer.enabled = true;
					
					frame6 = false;
					exit = true;
				}

				else if (exit)
				{
					//deactivate frame 2
					text6.gameObject.renderer.enabled = false;
					text61.gameObject.renderer.enabled = false;
					text62.gameObject.renderer.enabled = false;

					//close bubble
					bubble.gameObject.renderer.enabled = false;
					satannormal.renderer.enabled = false;

					//disable script
					isEnabled = false;
					anim.SetBool("Talk", false);

					//close minister
					MinisterNormal.gameObject.renderer.enabled = false;
					
					Dim.gameObject.renderer.enabled = false;
					
					//resume time and prevent superjumping
					Time.timeScale = 1;
					player.rigidbody2D.isKinematic = false;

					//Disappear with smoke
					StartCoroutine(poof (0.1f));
				}
			}
		}
		
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			//pause time and prevent superjumping
			Time.timeScale = 0;
			other.rigidbody2D.isKinematic = true;
			
			Dim.gameObject.renderer.enabled = true;
			MinisterNormal.gameObject.renderer.enabled = true;
			
			satannormal.renderer.enabled = true;
			satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-1);

			//frame 1
			bubble.gameObject.renderer.enabled = true;
			text1.gameObject.renderer.enabled = true;
			text11.gameObject.renderer.enabled = true;

			Destroy(gameObject.collider2D);
			isEnabled = true;
			anim.SetBool("Talk", true);
			
			
			frame2 = true; //play second frame
		}
	}

	IEnumerator poof(float waitTime) 
	{
		yield return new WaitForSeconds(0.4f);
		smoke.particleSystem.Play();
		yield return new WaitForSeconds(waitTime);
		gameObject.renderer.enabled = false;
	}
}
