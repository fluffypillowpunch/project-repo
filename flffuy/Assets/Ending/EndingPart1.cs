﻿using UnityEngine;
using System.Collections;

public class EndingPart1: MonoBehaviour {
	
	//Audio
	AudioSource audio;
	AudioSource peng;
	[SerializeField] private GameObject pengObject;
	AudioSource pullsword;
	[SerializeField] private GameObject pullswordObject;

	//hero sprite
	[SerializeField] private GameObject hero;
	[SerializeField] private GameObject herofinal;
	
	//Beginning Legend
	[SerializeField] private GameObject transparent;
	[SerializeField] private GameObject CG1;
	
	//CG Sprites
	[SerializeField] private GameObject lucinormal;
	[SerializeField] private GameObject luciangry;
	[SerializeField] private GameObject heronormal;
	[SerializeField] private GameObject herosmile;
	[SerializeField] private GameObject heroblack;
	[SerializeField] private GameObject satannormal;
	[SerializeField] private GameObject satancry;
	[SerializeField] private GameObject satansmile;
	[SerializeField] private GameObject bubble;
	
	//First Text dialogue
	[SerializeField] private GameObject dialogue1p1;
	[SerializeField] private GameObject dialogue1p2;
	[SerializeField] private GameObject dialogue1p3;
	[SerializeField] private GameObject dialogue1p31;
	[SerializeField] private GameObject dialogue1p4;
	[SerializeField] private GameObject dialogue1p41;
	[SerializeField] private GameObject dialogue1p5;
	[SerializeField] private GameObject dialogue1p51;
	[SerializeField] private GameObject dialogue1p6;
	[SerializeField] private GameObject dialogue1p61;
	[SerializeField] private GameObject dialogue1p62;
	[SerializeField] private GameObject dialogue1p7;
	[SerializeField] private GameObject dialogue1p8;
	[SerializeField] private GameObject dialogue1p9;
	[SerializeField] private GameObject dialogue1p10;
	[SerializeField] private GameObject dialogue1p11;
	[SerializeField] private GameObject dialogue1p12;
	[SerializeField] private GameObject dialogue1p13;
	[SerializeField] private GameObject dialogue1p131;
	[SerializeField] private GameObject dialogue1p14;
	[SerializeField] private GameObject dialogue1p15;
	//Text dialogue in CG picture 1
	[SerializeField] private GameObject dialogue2p1;
	[SerializeField] private GameObject dialogue2p2;
	[SerializeField] private GameObject dialogue2p3;
	[SerializeField] private GameObject dialogue2p31;
	[SerializeField] private GameObject dialogue2p4;
	[SerializeField] private GameObject dialogue2p41;
	[SerializeField] private GameObject dialogue2p42;
	[SerializeField] private GameObject dialogue2p5;
	[SerializeField] private GameObject dialogue2p51;
	[SerializeField] private GameObject dialogue2p6;
	[SerializeField] private GameObject dialogue2p61;
	[SerializeField] private GameObject dialogue2p7;
	[SerializeField] private GameObject dialogue2p71;
	[SerializeField] private GameObject dialogue2p8;

	bool dialogueframe2;
	bool dialogueframe3;
	bool dialogueframe4;
	bool dialogueframe5;
	bool dialogueframe6;
	bool dialogueframe7;
	bool dialogueframe8;
	bool dialogueframe9;
	bool dialogueframe10;
	bool dialogueframe11;
	bool dialogueframe12;
	bool dialogueframe13;
	bool dialogueframe14;
	bool dialogueframe15;
	bool dialogueframe16;
	bool dialogueframe17;
	bool dialogueframe18;
	bool dialogueframe19;
	bool dialogueframe20;
	bool dialogueframe21;
	bool dialogueframe22;
	bool dialogueframe23;
	bool dialogueframe24;
	bool closewindow;

	
	bool CG1frame1;
	bool CG1frame2;
	bool CG1frame3;
	
	
	
	
	
	
	// Use this for initialization
	void Start () {

		peng = pengObject.GetComponent<AudioSource> ();
		pullsword = pullswordObject.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown (KeyCode.UpArrow))
		{

			if (dialogueframe2)
			{
				//Turn off dialogue
				dialogue1p1.renderer.enabled = false;

				//Bring forward
				//satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);
				//Send back
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				herosmile.renderer.enabled = true;

				//Turn on Dialogue
				dialogue1p2.renderer.enabled = true;

				//Set to next frame
				dialogueframe2 = false;
				dialogueframe3 = true;
				
			}
			else if (dialogueframe3)
			{
				//Turn off dialogue
				dialogue1p2.renderer.enabled = false;
			
				//Turn on Dialogue
				dialogue1p3.renderer.enabled = true;
				dialogue1p31.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe3 = false;
				dialogueframe4 = true;
			}
			else if (dialogueframe4)
			{
				//Turn off dialogue
				dialogue1p3.renderer.enabled = false;
				dialogue1p31.renderer.enabled = false;

				//Send back
				//satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-2);
				heronormal.renderer.enabled = true;
				herosmile.renderer.enabled = false;
				satancry.renderer.enabled = true;

				//Turn on Dialogue
				dialogue1p4.renderer.enabled = true;
				dialogue1p41.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe4 = false;
				dialogueframe5 = true;
			}
			else if (dialogueframe5)
			{
				//Turn off dialogue
				dialogue1p4.renderer.enabled = false;
				dialogue1p41.renderer.enabled = false;
				
				//Turn on Dialogue
				dialogue1p5.renderer.enabled = true;
				dialogue1p51.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe5 = false;
				dialogueframe6 = true;
			}
			else if (dialogueframe6)
			{
				//Turn off dialogue
				dialogue1p5.renderer.enabled = false;
				dialogue1p51.renderer.enabled = false;

				satancry.renderer.enabled = false;
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);
				
				//Turn on Dialogue
				dialogue1p6.renderer.enabled = true;
				dialogue1p61.renderer.enabled = true;
				dialogue1p62.renderer.enabled = true;

				//Set to next frame
				dialogueframe6 = false;
				dialogueframe7 = true;
			}
			else if (dialogueframe7)
			{
				//Turn off dialogue
				dialogue1p6.renderer.enabled = false;
				dialogue1p61.renderer.enabled = false;
				dialogue1p62.renderer.enabled = false;

				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-4);

				dialogue1p7.renderer.enabled = true;

				//Set to next frame
				dialogueframe7 = false;
				dialogueframe8 = true;
			}
			else if (dialogueframe8)
			{
				//Turn off dialogue
				dialogue1p7.renderer.enabled = false;
			
				dialogue1p8.renderer.enabled = true;

				//Set to next frame
				dialogueframe8 = false;
				dialogueframe9 = true;
			}
			else if (dialogueframe9)
			{
				//Turn off dialogue
				dialogue1p8.renderer.enabled = false;
				
				dialogue1p9.renderer.enabled = true;
				
				satansmile.renderer.enabled = true;
				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-2);
				
				
				//Set to next frame
				dialogueframe9 = false;
				dialogueframe10 = true;
			}
			else if (dialogueframe10)
			{
				//Turn off dialogue
				dialogue1p9.renderer.enabled = false;
				
				dialogue1p10.renderer.enabled = true;
				satansmile.renderer.enabled = false;
				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-4);

				//Set to next frame
				dialogueframe10 = false;
				dialogueframe11 = true;
			}
			else if (dialogueframe11)
			{
				//Turn off dialogue
				dialogue1p10.renderer.enabled = false;
				
				dialogue1p11.renderer.enabled = true;
				pullsword.Play ();
				
				//Set to next frame
				dialogueframe11 = false;
				dialogueframe12 = true;
			}
			else if (dialogueframe12)
			{
				//Turn off dialogue
				dialogue1p11.renderer.enabled = false;

				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);
				heronormal.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-2);

				dialogue1p12.renderer.enabled = true;

				//Set to next frame
				dialogueframe12 = false;
				dialogueframe13 = true;
			}
			else if (dialogueframe13)
			{
				//Turn off dialogue
				dialogue1p12.renderer.enabled = false;
				
				dialogue1p13.renderer.enabled = true;
				dialogue1p131.renderer.enabled = true;

				//Set to next frame
				dialogueframe13 = false;
				dialogueframe14 = true;
			}
			else if (dialogueframe14)
			{
				//Turn off dialogue
				dialogue1p13.renderer.enabled = false;
				dialogue1p131.renderer.enabled = false;

				peng.Play();
				dialogue1p14.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe14 = false;
				dialogueframe15 = true;
			}
			else if (dialogueframe15)
			{
				//Turn off dialogue
				dialogue1p14.renderer.enabled = false;
				
				dialogue1p15.renderer.enabled = true;
				
				//Set to next frame
				dialogueframe15 = false;
				dialogueframe16 = true;
			}
			else if (dialogueframe16)
			{
				//Turn off dialogue
				dialogue1p15.renderer.enabled = false;
				satannormal.renderer.enabled = false;
				heronormal.renderer.enabled = false;
				transparent.renderer.enabled = false;
				bubble.renderer.enabled = false;
				CG1.renderer.enabled = true;

				dialogueframe16 = false;
				dialogueframe17 = true;
			}
			else if (dialogueframe17)
			{
				CG1.renderer.enabled = false;
				transparent.renderer.enabled = true;
				bubble.renderer.enabled = true;
				satannormal.renderer.enabled = true;
				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);

				heroblack.renderer.enabled = true;
				dialogue2p1.renderer.enabled = true;

				dialogueframe17 = false;
				dialogueframe18 = true;
			}  
			else if (dialogueframe18)
			{
				dialogue2p1.renderer.enabled = false;

				dialogue2p2.renderer.enabled = true;

				dialogueframe18 = false;
				dialogueframe19 = true;
			} 
			else if (dialogueframe19)
			{
				dialogue2p2.renderer.enabled = false;

				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);
				heroblack.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-2);

				dialogue2p3.renderer.enabled = true;
				dialogue2p31.renderer.enabled = true;
				
				dialogueframe19 = false;
				dialogueframe20 = true;
			} 
			else if (dialogueframe20)
			{
				dialogue2p3.renderer.enabled = false;
				dialogue2p31.renderer.enabled = false;

				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-2);
				heroblack.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-4);
				
				dialogue2p4.renderer.enabled = true;
				dialogue2p41.renderer.enabled = true;
				dialogue2p42.renderer.enabled = true;

				dialogueframe20 = false;
				dialogueframe21 = true;
			} 
			else if (dialogueframe21)
			{
				dialogue2p4.renderer.enabled = false;
				dialogue2p41.renderer.enabled = false;
				dialogue2p42.renderer.enabled = false;

				dialogue2p5.renderer.enabled = true;
				dialogue2p51.renderer.enabled = true;
				
				dialogueframe21 = false;
				dialogueframe22 = true;
			} 
			else if (dialogueframe22)
			{
				dialogue2p5.renderer.enabled = false;
				dialogue2p51.renderer.enabled = false;

				satannormal.transform.position = new Vector3 (satannormal.transform.position.x, satannormal.transform.position.y,-4);
				heroblack.transform.position = new Vector3 (heronormal.transform.position.x, heronormal.transform.position.y,-2);

				dialogue2p6.renderer.enabled = true;
				dialogue2p61.renderer.enabled = true;
				
				dialogueframe22 = false;
				dialogueframe23 = true;
			} 
			else if (dialogueframe23)
			{
				dialogue2p6.renderer.enabled = false;
				dialogue2p61.renderer.enabled = false;
			
				dialogue2p7.renderer.enabled = true;
				dialogue2p71.renderer.enabled = true;
				
				dialogueframe23 = false;
				dialogueframe24 = true;
			} 
			else if (dialogueframe24)
			{
				dialogue2p7.renderer.enabled = false;
				dialogue2p71.renderer.enabled = false;
				
				dialogue2p8.renderer.enabled = true;

				dialogueframe24 = false;
				closewindow = true;
			} 
			else if (closewindow)
			{
				dialogue2p8.renderer.enabled = false;
				//close window
				satannormal.renderer.enabled = false;
				heroblack.renderer.enabled = false;
				transparent.renderer.enabled = false;
				bubble.renderer.enabled = false;
				this.collider2D.enabled = false;
				Time.timeScale = 1;

				hero.renderer.enabled = false;
				herofinal.renderer.enabled = true;
				Destroy(this);
			}

		}
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			Time.timeScale = 0;
			transparent.renderer.enabled = true;
			bubble.renderer.enabled = true;
			satannormal.renderer.enabled = true;
			dialogue1p1.renderer.enabled = true;
			dialogueframe2 = true;
		}
	}




}