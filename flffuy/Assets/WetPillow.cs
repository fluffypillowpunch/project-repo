﻿using UnityEngine;
using System.Collections;

public class WetPillow : MonoBehaviour {
	

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "InstantDeath") 
		{
			Debug.Log("HIT!");
			StartCoroutine(death());
		}
	}

	IEnumerator death() 
	{
		yield return new WaitForSeconds (0.1f);
		this.renderer.enabled = false;
		yield return new WaitForSeconds (0.1f);
		this.renderer.enabled = true;
		yield return new WaitForSeconds (0.1f);
		this.renderer.enabled = false;
		yield return new WaitForSeconds (0.1f);
		this.renderer.enabled = true;
		yield return new WaitForSeconds (0.1f);
		this.renderer.enabled = false;
		yield return new WaitForSeconds (0.1f);
		this.renderer.enabled = true;
		yield return new WaitForSeconds (0.1f);
		this.renderer.enabled = false;
		yield return new WaitForSeconds (0.1f);
		this.renderer.enabled = true;
		yield return new WaitForSeconds (0.1f);
		this.renderer.enabled = false;
		yield return new WaitForSeconds (0.1f);
		this.renderer.enabled = true;
		Destroy (gameObject);
	}
}
