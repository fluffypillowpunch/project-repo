﻿using UnityEngine;
using System.Collections;

public class Satan_controller_script : MonoBehaviour {
	
	//Movement and animation variables
	public float maxSpeed = 10f;
	bool facingRight = true;
	Animator anim;
	bool isDead;
	bool canRun = true;
    int satanHP;
	
	//Jump Variables and ground check
	bool doubleJump = false;
	public float jumpForce = 1000f;
	bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.3f;
	public LayerMask whatIsGround;

	//Attack Variables
	bool attack;
	bool canAttack;
	float bounceOff;
	public GameObject pillow; 
	Vector2  pillowSpawn;
	Vector2  pillowVelocity;
	Vector2  pillowRotation;
	int pillowAmount;
	bool invulTimer;
	bool pillowTimer;

	//RESPAWN
	private bool beginning = true;
	private bool checkPoint1;
	private bool checkPoint2;
	private bool checkPoint3;
	private GameObject deleteThrownPillow1;
	private GameObject deleteThrownPillow2;
	private bool thrownPillow1 = false;


	//HUD
	[SerializeField] private GameObject health1;
	[SerializeField] private GameObject health2;
	[SerializeField] private GameObject health3;
	[SerializeField] private GameObject FeatherAmt;
	private GameObject pillowHUD1;
	private GameObject pillowHUD2;
	private GameObject gameOverHUD;
	private GameObject gameOverBubble;
	private GameObject gameOverRestart;

	int collectable = 0;

	void Start () {
	
		anim = GetComponent<Animator> ();  //Combine the animator component to the script
		satanHP = 3;
		pillow = GameObject.FindGameObjectWithTag("Pillow");
		pillowHUD1 = GameObject.FindGameObjectWithTag("PillowHUD1");
		pillowHUD2 = GameObject.FindGameObjectWithTag("PillowHUD2");
		gameOverHUD = GameObject.FindGameObjectWithTag("GameOverHUD");
		gameOverBubble = GameObject.FindGameObjectWithTag("GameOverBubble");
		gameOverRestart = GameObject.FindGameObjectWithTag("GameOverRestart");
		pillowAmount = 2;
	}

	void FixedUpdate () 
	{
		if (!isDead) 
		{
						//Checks for ground: is there a collider there?
						grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
						anim.SetBool ("Ground", grounded);

						if (grounded) {
								doubleJump = false;
								canAttack = true;
						} else 
								canAttack = false;

						//MOVING
						float move = Input.GetAxis ("Horizontal");		//Set left (-1) and right (1)

						if (canRun == true) {
								anim.SetFloat ("vSpeed", rigidbody2D.velocity.y); // Set animations for jumping
								anim.SetFloat ("Speed", Mathf.Abs (move));		//Set animations for running
								rigidbody2D.velocity = new Vector2 (move * maxSpeed, rigidbody2D.velocity.y);	//Move player left and right
						}

						bounceOff = (move * -1) * 2;

						//Correctly orientate the player (facing left or right?)
						if (move > 0 && !facingRight)
								Flip ();
						else if (move < 0 && facingRight)
								Flip ();


						//MELEE ATTACK
						attack = Input.GetKeyDown (KeyCode.Z);
						if (canAttack) {
								if (attack && move != 0) {
										rigidbody2D.velocity = new Vector2 (0.11f, 0);
										anim.SetBool ("Attack", true);
										StartCoroutine (attackPause (0.15f));
								} else if (attack) {
										anim.SetBool ("Attack", true);
								} else {
										anim.SetBool ("Attack", false);    //stop attack animation (prevent loop)
								}	
						}
						
						//RANGED ATTACK

						if (!pillowTimer)
						{
							if (pillowAmount == 1 || pillowAmount == 2)
							{
								
								if (facingRight)
								{
									pillowSpawn = new Vector3(transform.position.x + 1 , transform.position.y, transform.position.z); 
									pillowVelocity = new Vector3(300,150,-5);
						        }
						        else 
						        {
									pillowSpawn = new Vector3(transform.position.x -1 , transform.position.y, transform.position.z); 	
									pillowVelocity = new Vector3(-300,150,-5);
						        }
						                             
								if (Input.GetKeyDown(KeyCode.X)) //Throw pillow
					    		{
									pillowTimer = true;
									StartCoroutine(pillowCD(0.75f));
								}

							}
						}

				}
	}


	void Update()
	{	

		if ((grounded || !doubleJump) && Input.GetKeyDown (KeyCode.Space) && !isDead) 
		{
			anim.SetBool ("Ground", false); //Set animations for jumping

			if (!doubleJump && !grounded)  //Double Jumping
			{	   
				doubleJump = true; 									//Prevent another jump
				rigidbody2D.velocity = new Vector2 (0, 0.1f); 			//Reset velocity for even jumping
				rigidbody2D.AddForce (new Vector2 (0,jumpForce));	//perform double jump
			}
			else
				rigidbody2D.AddForce (new Vector2 (0, jumpForce)); //do regular jump
		}


		if (satanHP == 0 && !isDead) 
		{
			StartCoroutine (death (0.6f));
			health1.gameObject.renderer.enabled = false;
			health2.gameObject.renderer.enabled = false;
			health3.gameObject.renderer.enabled = false;
		}
		if (satanHP == 3) 
		{
			health3.renderer.enabled = true;
			health2.gameObject.renderer.enabled = false;
			health1.gameObject.renderer.enabled = false;
		}
		if (satanHP == 2) 
		{
			health1.renderer.enabled = true;
			health2.gameObject.renderer.enabled = false;
			health3.gameObject.renderer.enabled = false;
		}
		if (satanHP == 1) 
		{
			health2.gameObject.renderer.enabled = true;
			health1.gameObject.renderer.enabled = false;
			health3.gameObject.renderer.enabled = false;
		}
		if (pillowAmount == 2) 
		{
			pillowHUD1.gameObject.renderer.enabled = true;
			pillowHUD2.gameObject.renderer.enabled = true;
		}
		if (pillowAmount == 1) 
		{
			pillowHUD1.gameObject.renderer.enabled = true;
			pillowHUD2.gameObject.renderer.enabled = false;
		}
		if (pillowAmount == 0) 
		{
			pillowHUD1.gameObject.renderer.enabled = false;
			pillowHUD2.gameObject.renderer.enabled = false;
		}

		if (isDead) //Show game over message
		{
			gameOverHUD.gameObject.renderer.enabled = true;
			gameOverBubble.gameObject.renderer.enabled = true;
			gameOverRestart.gameObject.renderer.enabled = true;
		}

		//HUD for feathers
		calculateFeathers ();

		//RESTART LEVEL
		if (Input.GetKeyDown (KeyCode.R) && isDead) 
		{
			if(beginning)
			{
				Application.LoadLevel ("Main Levels");
				Time.timeScale = 1;
			}
			else if(checkPoint1)
			{
				checkPointSpawn(new Vector3 (29,4f,0));
			}
			else if(checkPoint2)
			{
				checkPointSpawn(new Vector3 (95 ,-4.4f,0));
			}
			else if(checkPoint3)
			{
				checkPointSpawn(new Vector3 (165 ,-11.5f,0));
			}

		}

	
	}
	
	void Flip() 
	{
		if (!isDead) 
		{
				//Allows the sprite to be facing left or right
				facingRight = !facingRight;
				Vector3 theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
		}
	}

	void checkPointSpawn(Vector3 SpawnLocation)
	{
		//spawn at location
		gameObject.transform.position = SpawnLocation;
		anim.SetBool ("IsDead", false);	

		isDead = false;
		gameOverHUD.gameObject.renderer.enabled = false;
		gameOverBubble.gameObject.renderer.enabled = false;
		gameOverRestart.gameObject.renderer.enabled = false;
		
		satanHP = 3;
		pillowAmount = 2;
		
		Destroy(deleteThrownPillow1);
		Destroy(deleteThrownPillow2);
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Enemy" && !invulTimer) 
		{
			invulTimer = true;
			anim.SetFloat ("Speed", 0);
			StartCoroutine (wait (0.6f));
			rigidbody2D.velocity = new Vector2(bounceOff, 1); 
			satanHP--;
		}

		if (other.gameObject.tag == "Fireball" && !invulTimer) 
		{
			invulTimer = true;
			anim.SetFloat ("Speed", 0);
			StartCoroutine (wait (0.6f));
			rigidbody2D.velocity = new Vector2(bounceOff, 1); 
			satanHP--;
		}

		if (other.gameObject.tag == "InstantDeath") 
		{
			anim.SetBool("IsDead", true);
			gameObject.rigidbody2D.velocity = new Vector2(0,0);
			isDead = true;
			satanHP = 0;
		}
		//StackEMs
		if (other.gameObject.tag == "StackEm2") 
		{
			if (collectable >= 2)
			{
				collectable-=2;
				GameObject stack1 = other.gameObject.transform.parent.gameObject;
				GameObject parent = stack1.transform.parent.gameObject;
				ParticleSystem poof = parent.GetComponent<ParticleSystem>();
				poof.Play ();
				Destroy(stack1);
			}
		}
        //StackEMs
        if (other.gameObject.tag == "StackEm3") 
        {
            if (collectable >= 3)
            {
                collectable-=3;
                GameObject stack1 = other.gameObject.transform.parent.gameObject;
                GameObject parent = stack1.transform.parent.gameObject;
                ParticleSystem poof = parent.GetComponent<ParticleSystem>();
                poof.Play ();
                Destroy(stack1);
            }
        }
        
        if (other.gameObject.tag == "Pillow") 
		{
			Destroy(other.gameObject);
			pillowAmount++;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "HealthRegen") 
		{
			Destroy(other.gameObject);
			if (satanHP < 3)
				satanHP++;
		}
		
		if (other.gameObject.tag == "Collectable") 
		{
			Destroy(other.gameObject);
			collectable++;
		}

		if (other.gameObject.tag == "Respawn1") 
		{
			Destroy(other.gameObject);
			checkPoint1 = true;
			beginning = false;
		}
		if (other.gameObject.tag == "Respawn2") 
		{
			Destroy(other.gameObject);
			checkPoint2 = true;
			checkPoint1 = false;
			beginning = false;
		}
		if (other.gameObject.tag == "Respawn3") 
		{
			Destroy(other.gameObject);
			checkPoint3 = true;
			checkPoint2 = false;
			beginning = false;
			checkPoint1 = false;
		}

	}
	void calculateFeathers()
	{
		TextMesh textmesh = FeatherAmt.GetComponent<TextMesh> ();
		if (collectable < 10)
			textmesh.text = "0" + collectable.ToString ();
		else
			textmesh.text = collectable.ToString ();
		
	}


	IEnumerator wait(float waitTime) 
	{
		anim.SetBool ("IsAttacked", true);
		canRun = false;
		yield return new WaitForSeconds(waitTime);
		anim.SetBool ("IsAttacked", false);
		canRun = true;
		invulTimer = false;
	}

	IEnumerator attackPause(float waitTime) 
	{
		canRun = false;
		yield return new WaitForSeconds(waitTime);
		canRun = true;
	}

	IEnumerator pillowCD(float waitTime) 
	{
		GameObject throwPillow;
		throwPillow = Instantiate(pillow, pillowSpawn, transform.rotation) as GameObject;
		throwPillow.gameObject.rigidbody2D.AddForce(pillowVelocity);
		pillowAmount--;
		yield return new WaitForSeconds(waitTime);
		pillowTimer = false;

		if (!thrownPillow1) 
		{
			Debug.Log ("Pillow set to 1");
			deleteThrownPillow1 = throwPillow;
			thrownPillow1 = true;
		} 
		else 
		{
			Debug.Log ("Pillow set to 2");
			deleteThrownPillow2 = throwPillow;
			thrownPillow1 = false;
		}
	}

	IEnumerator death(float waitTime) 
	{
		anim.SetFloat ("Speed", 0);
		StartCoroutine (wait (0.6f));
		rigidbody2D.velocity = new Vector2(bounceOff,0); 

		yield return new WaitForSeconds(waitTime);
		anim.SetBool("IsDead", true);
		gameObject.rigidbody2D.velocity = new Vector2(0,0);
		isDead = true;
	}

}

//REFERENCES
		// 1. [Unity Tutorial] Platform | Sidescroller 01   - Youtube Video
		// Author: Sebastian Lague
		// Date accessed: 13th April 2014
		// URL: https://www.youtube.com/watch?v=d3HEFiDFApI

		// 2. 2d Character Controllers (Unity Tutorial)
		// Author: Unity      Tutor: Mike Geig
		// Date accessed: 12th April 2014
		// http://unity3d.com/learn/tutorials/modules/beginner/2d/2d-controllers
		// Used for movement, flipping of sprites, animation, Jumping

		// 3. [Unity Tutorial] Platform | Sidescroller 04   - Youtube Video
		// Author: Sebastian Lague
		// Date accessed: 17th April 2014
		// URL: https://www.youtube.com/watch?v=zirvkEWxPXU

		//4. Throwing pillow - Instantiate
		//https://docs.unity3d.com/Documentation/ScriptReference/Object.Instantiate.html
		//25/4
		




