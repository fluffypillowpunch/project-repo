﻿using UnityEngine;
using System.Collections;

public class Monster4_animate : MonoBehaviour {

	Animator anim;
	bool canAttack = true;
	
	//Combat
	float monster4HP = 2;
	bool canBeAttacked = true;
	public GameObject fireball; 
	Vector2  fireSpawn;
	Vector2  fireVelocity;
	bool wasHit;
	
	[SerializeField] private GameObject smoke;
	[SerializeField] private GameObject bounce;
	AudioSource pillowthrown;

	float currentPos;

	//movement
	bool facingRight = false;
	bool check = true;

	// Use this for initialization
	void Start () {
		pillowthrown = GetComponent<AudioSource> ();
		anim = GetComponent<Animator> ();
		StartCoroutine (checkLeft (1));

	}
	
	// Update is called once per frame
	void Update () {
		if (monster4HP <= 0) {
			StartCoroutine (spriteDie (0.5f));
		}

		if (facingRight)
		{
			fireSpawn = new Vector3(transform.position.x + 1 , transform.position.y, transform.position.z); 
			fireVelocity = new Vector3(150,20,-5);
		}
		else 
		{
			fireSpawn = new Vector3(transform.position.x -1 , transform.position.y, transform.position.z); 	
			fireVelocity = new Vector3(-150,20,-5);
		}

		currentPos = gameObject.transform.position.y;
	}

	void Flip() 
	{
		//Allows the sprite to be facing left or right
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void OnTriggerEnter2D(Collider2D other)
	{

		if (other.gameObject.tag == "Player" && canAttack)
		{
			check= false;
			canAttack = false;
			gameObject.collider2D.enabled = false;
			StartCoroutine(Attacking ());
		}
		
		//Trigger being hit
		if (other.gameObject.tag == "Pillow")
		{
			if (monster4HP > 0 && canBeAttacked && (other.gameObject.transform.position.y > (currentPos -0.1))) 
			{
				monster4HP--;
				StartCoroutine (AttackedAnim (0.3f));
				StartCoroutine (canBeAttackedTimer(1));
				pillowthrown.Play();
			}	

		} 

		if (other.gameObject.tag == "JumpAttackingCollider")
		{
			//Trigger being hit
			if (monster4HP > 0 && canBeAttacked && (other.gameObject.transform.position.y > (currentPos +0.1))) 
			{
				monster4HP--;
				StartCoroutine (AttackedAnim (0.2f));
				StartCoroutine (canBeAttackedTimer(0.5f));
			} 
		}
	}
	

	
	IEnumerator checkLeft(float waitTime) 
	{
		yield return new WaitForSeconds (waitTime);
		if (check)
		{
			Flip ();
			StartCoroutine (checkRight (1f));
		}
	}
	

	IEnumerator checkRight(float waitTime) 
	{
		yield return new WaitForSeconds (waitTime);
		if (check)
		{
			Flip ();
			StartCoroutine (checkLeft (1f));
		}
	}
	
	IEnumerator Attacking() 
	{
		yield return new WaitForSeconds(0.5f);
		anim.SetBool ("Attack", true);

		//Create instance of evil shooter thingy
		GameObject shootfire;
		shootfire = Instantiate(fireball, fireSpawn, transform.rotation) as GameObject;
		shootfire.gameObject.rigidbody2D.AddForce(fireVelocity);
	
		yield return new WaitForSeconds(1f);
		anim.SetBool ("Attack", false);
		canAttack = true;
		gameObject.collider2D.enabled = true;
		check = true;
		StartCoroutine (checkLeft (1));
		
	}

	IEnumerator AttackedAnim(float waitTime) 
	{

		anim.SetBool ("Attack", true);
		yield return new WaitForSeconds(waitTime);
		if (monster4HP > 0)
			anim.SetBool ("Attack", false);

	}

	IEnumerator canBeAttackedTimer(float waitTime) 
	{
		canBeAttacked = false;
		yield return new WaitForSeconds(waitTime);
		canBeAttacked = true;
	}

	IEnumerator spriteDie(float waitTime) 
	{
		gameObject.tag = "Untagged";
		bounce.gameObject.collider2D.enabled = false;
		gameObject.collider2D.enabled = false;
		gameObject.collider2D.enabled = false;
		anim.SetBool ("Attack", true);
		yield return new WaitForSeconds(0.2f);
		smoke.particleSystem.Play();
		gameObject.renderer.enabled = false;
		yield return new WaitForSeconds(waitTime);
		Destroy (gameObject);
	}
}
